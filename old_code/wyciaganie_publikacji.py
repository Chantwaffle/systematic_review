#!"C:\Program Files\Python35\python.exe"

from Bio import Entrez
from Bio import Medline
import datetime
from impact_factor import impact_factor
from urllib.error import HTTPError



with open('ref_list.txt', 'r') as inf:
    ref_ids = [x.strip() for x in inf.readlines()]
    #print(ref_ids)

SEP = '\t'
NOW = datetime.datetime.now().strftime("%Y-%m-%d")
MAX_COUNT = 1000
TERM = ['ant(2")-Ia', "aph(3')-Ia", 'ant(2")-Ia PCR', "aph(3')-Ia PCR", 'ant(2")-Ia primers', "aph(3')-Ia primers"]
DB = 'pubmed'
CNT = 0
IF = '?'
with open('output.txt', 'w') as outf:
    for x in TERM:
        #print('Getting {0} publications containing {1}...'.format(MAX_COUNT, TERM))
        Entrez.email = 'A.X.dar@example.com'
        h = Entrez.esearch(db=DB, retmax=MAX_COUNT, term=x)
        result = Entrez.read(h)
        #print('Total number of publications containing {0}: {1}'.format(x, result['Count']))
        ids = result['IdList']
        #ids = ['26323624']
        ids = list(set(ids) - set(ref_ids))
        h = Entrez.efetch(db=DB, id=ids, rettype='medline', retmode='text')
        records = Medline.parse(h)

        for record in records:

            au = ', '.join(record.get('AU', '?'))
            ti = record.get('TI', '?')
            dp = record.get('DP', '?').split()[0]
            pmid = record.get('PMID', '?')
            jt = record.get('JT', '?').lower()
            ad = record.get('AD', '?').split('.')[0]
            ad_city = record.get('AD', '?').split('.')[0].split(',')[-1]
            vi = record.get('VI', '?')
            pg = record.get('PG', '?')
            ab = record.get('AB', '?')
            dep = record.get('DEP', '?')[:4]
            link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + str(pmid)
            try:
                IF = impact_factor[jt]
            except KeyError:
                IF = 'none'
            CNT += 1
            all = SEP.join([str(CNT), DB, NOW, str(CNT), x, jt, IF,  ti, au, ab, vi, pg, pmid, dep, ad, ad_city, link])
            ref = au + '. ' + dp.split()[0] + '. ' + ti + ' ' + jt + '. ' + vi + ': ' + pg

            outf.write(all + '\n')
