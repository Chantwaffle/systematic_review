#!"C:\Program Files\Python35\python.exe"

from Bio import Entrez
from Bio import Medline
import datetime
from impact_factor import impact_factor
from urllib.error import HTTPError



with open('ref_list.txt', 'r') as inf:
    ref_ids = [x.strip() for x in inf.readlines()]
    #print(ref_ids)

SEP = '\t'
NOW = datetime.datetime.now().strftime("%Y-%m-%d")
MAX_COUNT = 1000
TERM = ['mcr-1', 'mcr-2', 'mcr-3', 'mcr-4', 'mcr-5', 'mcr-6', 'mcr-7','mcr-8', 'mcr-9',
        'mcr-1 PCR', 'mcr-2 PCR', 'mcr-3 PCR', 'mcr-4 PCR', 'mcr-5 PCR', 'mcr-6 PCR', 'mcr-7 PCR', 'mcr-8 PCR',
        'mcr-9 PCR',
        'mcr-1 primers', 'mcr-2 primers', 'mcr-3 primers', 'mcr-4 primers', 'mcr-5 primers', 'mcr-6 primers',
        'mcr-7 primers',
        'mcr-8 primers', 'mcr-9 primers']
DB = 'pubmed'
CNT = 0
IF = '?'

for x in TERM:
	#print('Getting {0} publications containing {1}...'.format(MAX_COUNT, x))
	Entrez.email = 'A.X.dar@example.com'
	h = Entrez.esearch(db=DB, retmax=MAX_COUNT, term=x)
	result = Entrez.read(h)
	print('Total number of publications containing {0}:\t {1}'.format(x, result['Count']))
	ids = result['IdList']
	#ids = ['27532341']
	ids = list(set(ids) - set(ref_ids))
	h = Entrez.efetch(db=DB, id=ids, rettype='medline', retmode='text')
	records = Medline.parse(h)

"""for record in records:

    au = ' '.join(record.get('AU', '?'))
    ti = record.get('TI', '?')
    dp = record.get('DP', '?').split()[0]
    pmid = record.get('PMID', '?')
    jt = record.get('JT', '?').lower()
    ad = record.get('AD', '?').split('.')[0]
    ad_city = record.get('AD', '?').split('.')[0].split(',')[-1]
    vi = record.get('VI', '?')
    pg = record.get('PG', '?')
    ab = record.get('AB', '?')
    dep = record.get('DEP', '?')[:4]
    link = 'https://www.ncbi.nlm.nih.gov/pubmed/' + str(pmid)
    try:
        IF = impact_factor[jt]
    except KeyError:
        IF = 'none'
    CNT += 1
    all = SEP.join([str(CNT), DB, NOW, str(CNT), TERM, jt, IF,  ti, au, ab, vi, pg, pmid, dep, ad, ad_city, link])

    print(all)"""
