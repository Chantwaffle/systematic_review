"""
This script allows its users to assess the amount of
articles to review for scientific meta-review.

input:
@terms: list of terms to screen for in pubmed
@boundry_date: "%Y-%m-%d" boundry date of the systematic review.
    Articles after that date won't be taken into account
@ref_ids: list of already reviewed pubmed ids

output:
@res: csv table in which one line is one article metadata
"""

import argparse

from Bio import Entrez
from Bio import Medline
from impact_factor import impact_factor
from urllib.error import HTTPError

def get_impact_factor_dict():
    """
    Create a dictionary with mapped journal name with its
    impact factor. Input data is taken from preformatted file.
    """

    with open('if_list.txt', 'r') as inf:
        all_file = inf.readlines()
        impact_factor = {}

        for line in all_file:
            IF = line.split()[-1]
            journal = ' '.join(line.split()[1:-1]).lower()
            impact_factor[journal] = IF

    return impact_factor

def systematic_review(terms_file, boundry_date, ref_ids_file=None, DB='pubmed', MAX_COUNT=10000):
    """
    Screens pubmed with input terms. Excludes ref_ids from
    fetched data. Returns found article's metadata in tabular form.
    """

    if_dict = get_impact_factor_dict()

    with open(terms_file, 'r') as terms_raw:
        terms = [x.strip() for x in terms_raw.readlines()]
        terms.remove(' ')
        terms.remove('')
        terms = set(terms)
    print(f"Got {len(terms)} unique terms.")

    if ref_ids_file:
        with open(ref_ids_file, 'r') as ref_ids_raw:
            ref_ids = [x.strip() for x in ref_ids_raw.readlines()]
            ref_ids.remove(' ')
            ref_ids.remove('')
            ref_ids = set(ref_ids)
        print(f"Got {len(ref_ids)} unique ref ids.")
    else:
        print("No ref ids file supplied.")


    Entrez.email = 'example@example.com'
    for term in terms:
        response = Entrez.esearch(db=DB, retmax=MAX_COUNT, term=term)
        result = Entrez.read(response)
        pubmed_ids = result['IdList']

        pubmed_ids_to_get = list(set(pubmed_ids) - set(ref_ids))
        response = Entrez.efetch(db=DB, id=pubmed_ids_to_get, rettype='medline', retmode='text')
        records = Medline.parse(response)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("terms_file", help="Path to file with terms to use for pubmed screening. One term per line.")
    parser.add_argument("boundry_date", help="'%Y-%m-%d' boundry date of the systematic review. Articles after that date won't be taken into account")
    parser.add_argument("-ref_ids_file", help="Path to file with already reviewed pubmed ids. One id per line.")

    args = parser.parse_args()
    print(args.terms_file)
    systematic_review(args.terms_file, args.boundry_date, args.ref_ids_file)
